import org.junit.Test;
import org.mockito.Mockito;
import repository.UserRepository;
import repository.UserRepositoryImpl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UserRepositoryImplTest {

    private UserRepository userRepositoryMock = Mockito.mock(UserRepository.class);

    private String name = "test";
    private String email = "test@gmail.com";
    private String password = "qwert3!";
    private String birthday = "23 September";

    @Test
    public void shouldReturnTrueIfUserExist() {
        Mockito.when(userRepositoryMock.userIsExist("test22@gmail.com")).thenReturn(true);

        assertTrue(UserRepositoryImpl.checkUserIsExist(userRepositoryMock, "test22@gmail.com"));
        Mockito.verify(userRepositoryMock).userIsExist("test22@gmail.com");
    }

    @Test
    public void shouldReturnFalseIfUserNotExist() {
        Mockito.when(userRepositoryMock.userIsExist("test@gmail.com")).thenReturn(false);

        assertFalse(UserRepositoryImpl.checkUserIsExist(userRepositoryMock, "test@gmail.com"));
        Mockito.verify(userRepositoryMock).userIsExist("test@gmail.com");
    }

    @Test
    public void shouldReturnTrueWhenNewUserIsCreated() {
        Mockito.when(userRepositoryMock.createNewUser(name, email, password, birthday)).thenReturn(true);

        assertTrue(UserRepositoryImpl.createNewUser(userRepositoryMock, name, email, password, birthday));
        Mockito.verify(userRepositoryMock).createNewUser(name, email, password, birthday);
    }

    @Test
    public void shouldReturnFalseWhenNewUserIsNotCreated() {
        Mockito.when(userRepositoryMock.createNewUser(name, email, password, birthday)).thenReturn(false);

        assertFalse(UserRepositoryImpl.createNewUser(userRepositoryMock, name, email, password, birthday));
        Mockito.verify(userRepositoryMock).createNewUser(name, email, password, birthday);
    }

}
