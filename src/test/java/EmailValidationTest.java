import org.junit.Assert;
import org.junit.Test;
import validators.EmailValidator;

import static org.junit.Assert.assertTrue;

public class EmailValidationTest {

    @Test
    public void mayHaveLetters(){
        Assert.assertEquals(true, EmailValidator.isValid("qwerty@"));
    }

    @Test
    public void mayHaveNumbers() {
        Assert.assertEquals(true, EmailValidator.isValid("weww33@"));
    }

    @Test
    public void mayHaveDots() {
        assertTrue(EmailValidator.isValid("weww.fd@"));
    }

    @Test
    public void mayHaveDash() {
        Assert.assertEquals(true, EmailValidator.isValid("weww-fd@"));
    }

    @Test
    public void mayHaveUnderscore() {
        Assert.assertEquals(true, EmailValidator.isValid("weww_fd@"));
    }

    @Test
    public void shouldHaveOneEmailSymbol() {
        Assert.assertEquals(true, EmailValidator.isValid("qwerty@"));
        Assert.assertEquals(false, EmailValidator.isValid("qw@erty@"));
    }


}
