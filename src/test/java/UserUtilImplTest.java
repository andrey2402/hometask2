import org.junit.Test;
import utils.UserUtilImpl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UserUtilImplTest {

    @Test
    public void shouldReturnTrueWhenDataAreValid() {
        assertTrue(UserUtilImpl.validateUserData
                ("test", "qwert123@gmail.com", "qwerty2#", "23 September"));
    }

    @Test
    public void shouldReturnFalseWhenNameIsEmpty() {
        assertFalse(UserUtilImpl.validateUserData
                ("", "qwert123@gmail.com", "qwerty2#", "23 September"));
    }

    @Test
    public void shouldReturnFalseWhenBirthdayIsEmpty() {
        assertFalse(UserUtilImpl.validateUserData
                ("test", "qwert123@gmail.com", "qwerty2#", ""));
    }

    @Test
    public void shouldReturnFalseWhenEmailInvalid() {
        assertFalse(UserUtilImpl.validateUserData
                ("test", "qwer@t123@gmail.com", "qwerty2#", "23 September"));
    }

    @Test
    public void shouldReturnFalseWhenPasswordInvalid() {
        assertFalse(UserUtilImpl.validateUserData
                ("test", "qwert123@gmail.com", "qwerty", "23 September"));
    }

}


