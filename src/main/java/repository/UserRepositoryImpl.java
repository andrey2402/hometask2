package repository;

import utils.UserUtilImpl;

public class UserRepositoryImpl {

    public static boolean checkUserIsExist(UserRepository userRepository, String email){

        return userRepository.userIsExist(email);
    }

    public static boolean createNewUser(UserRepository userRepository, String name, String email, String password, String birthday){

        if(!UserUtilImpl.validateUserData(name, email, password, birthday)){
            return false;
        }

        return userRepository.createNewUser(name, email, password, birthday);
    }

}
