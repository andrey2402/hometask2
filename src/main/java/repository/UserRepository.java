package repository;

public interface UserRepository {

    boolean userIsExist(String email);

    boolean createNewUser(String name, String email, String password, String birthday);

}
