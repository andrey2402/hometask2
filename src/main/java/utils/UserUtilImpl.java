package utils;

import validators.EmailValidator;
import validators.PasswordValidator;

public class UserUtilImpl {

    public static boolean validateUserData(String name, String email, String password, String birthday){

        if(name.isEmpty()){
            return false;
        }

        if(birthday.isEmpty()){
            return false;
        }

        if(!EmailValidator.isValid(email)){
            return false;
        }

        if(!PasswordValidator.isValid(password)){
            return false;
        }

       return true;
    }

}
