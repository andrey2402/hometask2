package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {

    private static final Pattern pattern = Pattern.compile(".*[!#$?%^&*()+=></\';:,{}].*");

    public static boolean isValid(String email) {

        int valid = 0;

        if (!email.matches(".*[a-zA-Z]+.*")) {
            return false;
        }

        for (int i = 0; i < email.length(); i++) {
            if (email.charAt(i) == '@') {
                valid++;
            }
        }

        if (valid != 1) {
            return false;
        }

        Matcher m = pattern.matcher(email);
        if (m.matches()) {
            return false;
        }

        return true;
    }

}
