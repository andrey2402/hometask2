package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {

    private static final int MIN_LENGTH = 5;
    private static final int MAX_LENGTH = 10;
    private static final Pattern pattern = Pattern.compile(".*[!@#$?%^&*()_+=-></\';:.,{}].*");

    public static boolean isValid(String password) {

        if (password.contains(" ")) {
            return false;
        }

        if (!password.matches(".*[a-zA-Z]+.*")) {
            return false;
        }

        if (!password.matches(".*[0-9].*")) {
            return false;
        }

        Matcher m = pattern.matcher(password);
        if (!m.matches()) {
            return false;
        }

        return password.length() >= MIN_LENGTH && password.length() <= MAX_LENGTH;
    }

}
